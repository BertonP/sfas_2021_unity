﻿Shader "Custom/Dissolve"{
	Properties
	{
		
		//Dissolve properties
		_DissolveTexture("Dissolve Texutre", 2D) = "white" {}
		_Amount("Dissolve Amount", Range(0,1)) = 0.0
	}

	SubShader
	{
		Tags {"RenderType" = "Opaque"}
		LOD 200

		CGPROGRAM

		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		struct Input
		{
			float2 uv_DissolveTexture;
		};
		
		//Dissolve
		sampler2D _DissolveTexture;
		half _Amount;
		
		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			//dissolve function
			half dissolve_value = tex2D(_DissolveTexture, IN.uv_DissolveTexture).r;
	
			clip(dissolve_value - _Amount);
		}

		ENDCG
	}
	Fallback "Standard"

}
