﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Note", menuName = "Notes/New Note", order = 0)]
public class Note : ScriptableObject
{
    new public string name = "Item";
    public string Title;
    public string Description;
    public string Text;
}
