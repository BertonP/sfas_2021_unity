﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NotesUI : MonoBehaviour
{
    private PlayerNotes _playerNotes;
    private bool _disabled = false;

    [SerializeField] private GameObject _notesToDisable;
    [SerializeField] private GameObject _notePrefab;
    [SerializeField] private TextMeshProUGUI _noteTitle;
    [SerializeField] private TextMeshProUGUI _noteText;

    private bool _showingNoteInfo = false;
     
    public void Init(PlayerNotes playerNotes)
    {
        _playerNotes = playerNotes;
        _playerNotes.OnNoteAddCallback += AddNoteUI;
        _notesToDisable.SetActive(false);
    }

    public void AddNoteUI(Note note)
    {
        GameObject newNoteUI = GameObject.Instantiate(_notePrefab);
        newNoteUI.transform.SetParent(transform, false);

        Button noteButton = newNoteUI.transform.Find("Button").GetComponent<Button>();
        noteButton.onClick.AddListener(() => DisplayNoteText(note));
        //noteButton.OnDeselect(() => DisplayNoteText(note));
        newNoteUI.transform.Find("Button").Find("Text").GetComponent<TextMeshProUGUI>().text = note.Description;
    }

    public void ChangeVisibility()
    {
        _disabled = !_disabled;
        _notesToDisable.SetActive(_disabled);
    }

    public void DisplayNoteText(Note note)
    {
        //I have no time to think of a good way of doing this. If its the same title, this one is already showing. Stop showing it.
        if(_noteTitle.text == note.Title) 
        {
            
            _noteTitle.text = "";
            _noteText.text = "";
        }
        else
        {
            _noteTitle.text = note.Title;
            _noteText.text = note.Text;
        }
    }

}
