﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : MonoBehaviour
{
    private AIText _aIText;
    [SerializeField] private StoryData _data;
    [SerializeField] private ConsoleInteractable _console;


    void Start()
    {
        _aIText = AIText.Instance;
        _console.Data = _data;
        _aIText.CurrentConsole = _console;
        _console.ShouldDisplay = true;
        Invoke(nameof(EnableCursor), 10.0f);
    }

    public void StartGame()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        _aIText.ClearScreen();
        GameObject.Destroy(gameObject);
    }

    private void EnableCursor()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
