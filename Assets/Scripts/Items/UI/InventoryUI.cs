﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    private Inventory _inventory;
    private bool _disabled = false;

    [SerializeField] private GameObject _inventoryToDisable;
    [SerializeField] private GameObject _itemPrefab;

    public void Init(Inventory inventory)
    {
        _inventory = inventory;
        _inventory.OnItemAddCallback += AddItemUI;
        _inventoryToDisable.SetActive(false);
    }

    public void AddItemUI(Item item)
    {
        GameObject newItemUI = GameObject.Instantiate(_itemPrefab);
        newItemUI.transform.SetParent(transform,false);
        newItemUI.transform.Find("Icon").GetComponent<Image>().sprite = item.icon;
    }

    public void ChangeVisibility()
    {
        _disabled = !_disabled;
        _inventoryToDisable.SetActive(_disabled);
    }



}
