﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Items/New Item", order = 0)]
public class Item : ScriptableObject
{
    new public string name = "Item";
    public Sprite icon = null;
}
