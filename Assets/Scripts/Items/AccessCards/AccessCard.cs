﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AccessCard", menuName = "Items/AccessCard", order = 1)]
public class AccessCard : Item
{
    public enum CardType
    {
        Green,
        Yellow,
        Red,
    }

    [SerializeField] private CardType _cardType;

    public CardType Type { get { return _cardType; } }
}
