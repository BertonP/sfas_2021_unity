﻿using System.Collections;
using UnityEngine;

public class AIText : MonoBehaviour
{
    //[SerializeField] public StoryData Data;

    public ConsoleInteractable CurrentConsole;

    private TextDisplay _output;
    private BeatData _currentBeat;
    private WaitForSeconds _wait;

    #region Singleton
    public static AIText Instance { get; private set; }

    void Awake()
    {
        transform.SetParent(null,false);
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);

        _output = GetComponentInChildren<TextDisplay>();
        _currentBeat = null;
        _wait = new WaitForSeconds(0.5f);
    }
    #endregion


    private void Update()
    {
        if (CurrentConsole == null || !CurrentConsole.ShouldDisplay) { return; }

        if (_output.IsIdle)
        {
            if (_currentBeat == null) // If its the first one
            {
                DisplayBeat(1);
            }
            else
            {
                if (CurrentConsole.CanInteract)
                {
                    UpdateInput();
                }
            }
        }
    }

    private void UpdateInput()
    {
        KeyCode alpha = KeyCode.Alpha1;
        KeyCode keypad = KeyCode.Keypad1;

        for (int count = 0; count < _currentBeat.Decision.Count; ++count)
        {
            if (alpha <= KeyCode.Alpha9 && keypad <= KeyCode.Keypad9)
            {
                if (Input.GetKeyDown(alpha) || Input.GetKeyDown(keypad))
                {
                    ChoiceData choice = _currentBeat.Decision[count];
                    DisplayBeat(choice.NextID);
                    break;
                }
            }

            ++alpha;
            ++keypad;
        }

    }

    private void DisplayBeat(int id)
    {
        BeatData data = CurrentConsole.Data.GetBeatById(id);
        StartCoroutine(DoDisplay(data));
        _currentBeat = data;
    }

    private IEnumerator DoDisplay(BeatData data)
    {
        _output.Clear();

        while (_output.IsBusy)
        {
            yield return null;
        }

        _output.Display(data.DisplayText);

        while (_output.IsBusy)
        {
            yield return null;
        }

        for (int count = 0; count < data.Decision.Count; ++count)
        {
            ChoiceData choice = data.Decision[count];
            _output.Display(string.Format("{0}: {1}", (count + 1), choice.DisplayText));

            while (_output.IsBusy)
            {
                yield return null;
            }
        }

        if (data.Decision.Count > 0)
        {
            _output.ShowWaitingForInput();
        }
        else
        {
            CurrentConsole.FinishedDisplay();
        }
    }

    public void BigScreenTextSize()
    {
        _output.BigScreenTextSize();
    }

    public void ClearScreen()
    {
        StopAllCoroutines();
        _output.Clear();
        this.CurrentConsole = null;
        _currentBeat = null;
    }

    public bool IsOutputBusy()
    {
        return _output.IsBusy;
    }
}
