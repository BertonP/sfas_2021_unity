﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    private AIText _aiText;
    public ConsoleInteractable Console;
    [SerializeField] public StoryData Data;

    private void Start()
    {
        _aiText = AIText.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Entered collider");
            PlayerInteraction player = other.gameObject.GetComponent<PlayerInteraction>();
            if (!player.HasLaptop())
            {
                Debug.Log("Don't have laptop");
                Console.Data = Data;
                StartCoroutine(ShowFinalText());
            }
        }
    }

    private IEnumerator ShowFinalText()
    {
        _aiText.ClearScreen();

        while (_aiText.IsOutputBusy())
        {
            yield return null;
        }
        _aiText.CurrentConsole = Console;
        Console.ShouldDisplay = true;
        _aiText.BigScreenTextSize();
    }
}
