﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockDoor : MonoBehaviour
{
    [SerializeField] Door _door;
    private AIText _aiText;

    private void Start()
    {
        _aiText = AIText.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            _door.IsLocked = true;
            _aiText.ClearScreen();
        }
    }
}
