﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] protected bool _isLocked = false;
    protected bool _isOpen = false;
    public bool ConsoleFinished = false;
    public bool CardAccessGranted = false;

    public bool IsLocked { 
        get { return !ConsoleFinished || !CardAccessGranted || _isLocked; } 
        set { _isLocked = value; } }
}
