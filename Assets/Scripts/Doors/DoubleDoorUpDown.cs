﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleDoorUpDown : Door
{
    [SerializeField] private PlayerController _player;
    [SerializeField] private GameObject _upperDoor;
    [SerializeField] private GameObject _lowerDoor;
    [SerializeField] private float _nearDistance = 3f;
    [SerializeField] private float _openSpeed = 0.35f;
    private float _openAmount = 1.75f;

    private int _lastAnimId = -1;

    void Update()
    {
        
        if(IsLocked) { return; }

        bool isNear = Vector3.Distance(_player.transform.position, this.transform.position) < _nearDistance;

        if (isNear && !_isOpen) //open double door
        {
            //Not the perfect behaviour, but at least the user can't bug the doors permanently
            // Or at least not easily

        if(_lastAnimId == -1 || !AnimationInProgress(_lastAnimId)) { 

            MoveY(_upperDoor,_openAmount);
            _lastAnimId = MoveY(_lowerDoor, -(_openAmount) );
            _isOpen = !_isOpen;
        }
        } 
        else if(!isNear && _isOpen)//close double door
        {
            if (_lastAnimId == -1 || !AnimationInProgress(_lastAnimId)) {
                
                MoveY(_upperDoor, -(_openAmount));
                _lastAnimId = MoveY(_lowerDoor, _openAmount);
                _isOpen = !_isOpen;
            }
        }
    }

    private int MoveY(GameObject gameObject, float moveAmount)
    {
        LTDescr tDescr = LeanTween.moveY(gameObject, gameObject.transform.position.y + moveAmount, _openSpeed).setEase(LeanTweenType.easeInQuad);
        return tDescr.id;
    }

    private bool AnimationInProgress(int id)
    {
        return LeanTween.isTweening(id);
    }
}
