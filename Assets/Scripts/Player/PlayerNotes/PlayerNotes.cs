﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNotes
{
    public delegate void OnNoteAdd(Note note);
    public OnNoteAdd OnNoteAddCallback;

    public List<Note> Notes;

    public PlayerNotes()
    {
        this.Notes = new List<Note>();
    }

    public void Add(Note note)
    {
        this.Notes.Add(note);

        if (OnNoteAddCallback != null)
        {
            OnNoteAddCallback.Invoke(note);
        }
    }
}
