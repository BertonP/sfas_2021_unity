﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Code from Brackeys's tutorial video.
 * Fixed bug where mouseX and Y were being multiplied by Time.deltaTime
 */

public class PlayerMouseLook : MonoBehaviour
{
    [SerializeField] private float _mouseSens = 3f;
    [SerializeField] private Transform _playerBody;
    private float _xRotation;

    private bool _disabled = false;

    void Start()
    {
        _xRotation = 0f;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        if(_disabled) { return; }

        float mouseX = Input.GetAxis("Mouse X") * _mouseSens;
        float mouseY = Input.GetAxis("Mouse Y") * _mouseSens;

        _xRotation -= mouseY;
        _xRotation = Mathf.Clamp(_xRotation, -90f, 90f);


        transform.localRotation = Quaternion.Euler(_xRotation, 0, 0);
        _playerBody.Rotate(Vector3.up * mouseX); 
    }

    public void ChangeDisabled()
    {
        if (_disabled)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        } else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        _disabled = !_disabled;
    }
}
