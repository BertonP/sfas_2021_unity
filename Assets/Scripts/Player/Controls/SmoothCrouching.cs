﻿using UnityEngine;
using System.Collections;

namespace Assets
{
    public class SmoothCrouching
    {

        [SerializeField] private float _crouchedHeightFactor = 0.75f;
        [SerializeField] private float _interpolationTime = 0.25f;


        private Collider _playerCollider;
        private Vector3 _crouchedScale;
        private Vector3 _initialScale;
        

        private bool _isCrouching;
        private int _interpolationFrames;
        private int _currentFrameCount;

        public SmoothCrouching(Collider playerCollider)
        {
            _playerCollider = playerCollider;
            _isCrouching = false;
            float initialHeight = playerCollider.transform.localScale.y;
            float crouchedHeight = initialHeight * _crouchedHeightFactor;

            _interpolationFrames = (int) (1 / Time.deltaTime * _interpolationTime);
            _currentFrameCount = _interpolationFrames;

            Vector3 playerScale = playerCollider.transform.localScale;
            _crouchedScale = new Vector3(playerScale.x, crouchedHeight, playerScale.z);
            _initialScale = new Vector3(playerScale.x, initialHeight, playerScale.z);
        }

        public void ChangeCrouching()
        {
            if(_currentFrameCount <= _interpolationFrames)
            {
                _currentFrameCount = _interpolationFrames - _currentFrameCount;
            }
            else
            {
                _currentFrameCount = 0;
            }
            _isCrouching = !_isCrouching;
        }

        public void Update()
        {
            if(_currentFrameCount <= _interpolationFrames)
            {
                float interpolationRatio = (float)_currentFrameCount / _interpolationFrames;
                if (_isCrouching)
                {
                    _playerCollider.transform.localScale = Vector3.Lerp(_initialScale, _crouchedScale, interpolationRatio);
                }
                else
                {
                    _playerCollider.transform.localScale = Vector3.Lerp(_crouchedScale, _initialScale, interpolationRatio);
                }
                _currentFrameCount++;
            }
        }
    }
}