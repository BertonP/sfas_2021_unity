﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Interactable;

public class PlayerInteraction : MonoBehaviour
{

    [SerializeField] Camera _playerCamera;
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private PlayerMouseLook _playerMouseLook;


    private LayerMask _interactableMask;
    private float _maxInteractableDistance = 10f;

    private Interactable _lastInteractable;
    private Interactable _currentInteractable;

    private Inventory _inventory;
    [SerializeField] private InventoryUI _inventoryUI;

    private PlayerNotes _playerNotes;
    [SerializeField] private NotesUI _notesUI;

    private bool _disabled = false;
    private void Start()
    {
        _interactableMask = LayerMask.GetMask("Interactable");

        _inventory = new Inventory();
        _inventoryUI.Init(_inventory);

        _playerNotes = new PlayerNotes();
        _notesUI.Init(_playerNotes);
    }

    // Update is called once per frame
    void Update()
    {
        if (!_disabled)
        {

            HandleInteraction();
        }

        HandleInput();
    }

    private void HandleInteraction()
    {
        Vector3 cameraForward = _playerCamera.transform.forward;
        Vector3 cameraPosition = _playerCamera.transform.position;
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(cameraPosition, cameraForward, out hit, _maxInteractableDistance, _interactableMask))
        {
            //Hit an interactable

            //Debug.DrawRay(cameraPosition, cameraForward * hit.distance, Color.yellow);

            _currentInteractable = hit.collider.GetComponent<Interactable>();

            //if not in interaction range, return
            if(hit.distance > _currentInteractable._interactionRange) { return; }

            if (_lastInteractable != _currentInteractable)
            {
                if (_lastInteractable != null)
                {
                    _lastInteractable.LookAt();
                }
                _lastInteractable = _currentInteractable;
                _currentInteractable.LookAt();
            }
        }
        else
        {
            //Debug.DrawRay(cameraPosition, cameraForward * 1000, Color.green);
            if (_lastInteractable != null)
            {
                _lastInteractable.StopLooking();
                _lastInteractable = null;
            }
            _currentInteractable = null;
        }
    }

    private void HandleInput()
    {
        bool changeInventory = Input.GetButtonDown("Inventory");
        bool changeNotes = Input.GetButtonDown("Notes");

        if (changeInventory || changeNotes)
        {
            if(changeInventory)
            {
                _inventoryUI.ChangeVisibility();
            } else
            {
                _notesUI.ChangeVisibility();
            }
            _playerController.ChangeDisabled();
            _playerMouseLook.ChangeDisabled();
            _disabled = !_disabled;
        }

        if (_disabled) { return; }

        if (Input.GetButtonDown("Interact"))
        {
            if(_currentInteractable != null)
            {
                InteractWith(_currentInteractable);
            }
        }
    }

    private void InteractWith(Interactable interactable)
    {
        switch (interactable.Type)
        {
            case InteractableType.Item:
                interactable.Interact(_inventory);
                break;
            case InteractableType.Note:
                interactable.Interact(_playerNotes);
                break;
            case InteractableType.CardAccess:
                interactable.Interact(_inventory);
                break;
            default:
                interactable.Interact();
                break;

        }
    }

    public bool HasLaptop()
    {
        //no time...
        bool foundLaptop = false;
        foreach(Item item in _inventory.Items)
        {
            if(item.name == "Laptop")
            {
                foundLaptop = true;
            }
        }
        return foundLaptop;
    }
}
