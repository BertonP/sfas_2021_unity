﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    public enum InteractableType
    {
        Normal,
        Item,
        Note,
        CardAccess,
    }


    [SerializeField] public float _interactionRange = 1.5f;

    public InteractableType Type;

    public virtual void LookAt(){ }
    public virtual void StopLooking() { }

    public virtual void Interact() { }
    public virtual void Interact(Inventory inventory) { }

    public virtual void Interact(PlayerNotes playerNotes) { }
}
