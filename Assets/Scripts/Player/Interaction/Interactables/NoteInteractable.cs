﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteInteractable : HightLightInteractable
{
    [SerializeField] public Note Note;

    public override void Interact(PlayerNotes playerNotes)
    {
        playerNotes.Add(Note);
        GameObject.Destroy(gameObject);
    }

}
