﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardInteractable : HightLightInteractable
{
    [SerializeField] private AccessCard _card;


    public override void Interact(Inventory inventory)
    {
        inventory.Add(_card);
        GameObject.Destroy(gameObject);
    }

}
