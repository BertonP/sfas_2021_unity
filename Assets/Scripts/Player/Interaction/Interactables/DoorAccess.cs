﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static AccessCard;

public class DoorAccess : HightLightInteractable
{


    [SerializeField] private Door _door;
    [SerializeField] private CardType _cardType;
    private Material _material;

    private void Awake()
    {
        //I get it like this so I don't change the material itself, for all others as well
        _material = gameObject.GetComponent<Renderer>().material;
    }

    public override void Interact(Inventory inventory)
    {
        if (inventory.HasAccessCardOfType(_cardType)){
            _door.CardAccessGranted = true;
            //Value copied from inspector
            _material.SetColor("_EmissionColor", new Color(0.0466624908f, 3.43034577f, 0, 1));
            gameObject.layer = 0; //Stop hightlight and further interactions
        }
    }
}
