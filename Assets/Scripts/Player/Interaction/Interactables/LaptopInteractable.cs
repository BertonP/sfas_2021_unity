﻿using cakeslice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaptopInteractable : HightLightInteractable
{
    [SerializeField] private Item _item;

    [SerializeField] private Outline _secondOutline;
    public override void LookAt()
    {
        outline.eraseRenderer = !outline.eraseRenderer;
        _secondOutline.eraseRenderer = !_secondOutline.eraseRenderer;
    }

    public override void StopLooking()
    {
        outline.eraseRenderer = !outline.eraseRenderer;
        _secondOutline.eraseRenderer = !_secondOutline.eraseRenderer;
    }

    public override void Interact(Inventory inventory)
    {
        inventory.Add(_item);
        GameObject.Destroy(gameObject);
    }
}
