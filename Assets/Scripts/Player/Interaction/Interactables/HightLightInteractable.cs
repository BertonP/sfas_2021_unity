﻿using cakeslice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HightLightInteractable : Interactable
{
    [SerializeField] protected Outline outline;
    public override void LookAt()
    {
        outline.eraseRenderer = !outline.eraseRenderer;
    }

    public override void StopLooking()
    {
        outline.eraseRenderer = !outline.eraseRenderer;
    }
}
