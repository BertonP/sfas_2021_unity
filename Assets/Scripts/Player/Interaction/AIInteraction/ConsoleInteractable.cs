﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleInteractable : Interactable
{
    private AIText _aiText;
    private static GameObject _player;
    [SerializeField] private float _showTextRange = 2f;

    [SerializeField] public Door door;
    [SerializeField] public StoryData Data;
    [HideInInspector] public bool ShouldDisplay = false;
    [HideInInspector] public bool CanInteract = false;

    [HideInInspector] public bool Finished = false;

    private void Start()
    {
        _aiText = AIText.Instance;
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {

        //It's innefficient to update all consoles every update, but I don't have time for this
        //If I could, I would maybe save an ordered list of consoles on PlayerInteraction, and an index "CurrentConsole".
        //Then check the current console.

        //This is because I want the text to start showing even without the playing looking directly at console.
        Vector3 playerPos = _player.transform.position;

        if(Vector3.Distance(playerPos, transform.position) <= _showTextRange)
        {
            if(_aiText.CurrentConsole != this)
            {

                _aiText.CurrentConsole = this;
            }
            ShouldDisplay = true;
        }
    }

    public override void LookAt()
    {
        CanInteract = true;
    }

    public override void StopLooking()
    {
        CanInteract = false;
    }

    public void FinishedDisplay()
    {
        if(door != null)
        {
            door.ConsoleFinished = true;
        }
    }
}
