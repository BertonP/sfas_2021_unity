﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static AccessCard;

public class Inventory
{
    public delegate void OnItemAdd(Item item);
    public OnItemAdd OnItemAddCallback;

    public List<Item> Items;
    
    public Inventory()
    {
        this.Items = new List<Item>();
    }

    public void Add (Item item)
    {
        this.Items.Add(item);

        if(OnItemAddCallback != null)
        {
            OnItemAddCallback.Invoke(item);
        }
    }

    public List<AccessCard> GetAccessCards()
    {
        List<AccessCard> cards = new List<AccessCard>();
        foreach(Item item in Items)
        {
            if(item is AccessCard)
            {
                cards.Add((AccessCard)item);
            }
        }
        return cards;
    }

    public bool HasAccessCardOfType(CardType cardType)
    {
        foreach (Item item in Items)
        {
            if (item is AccessCard)
            {
                if( ((AccessCard)item).Type == cardType)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
