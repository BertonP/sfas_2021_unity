﻿using Assets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private CharacterController _controller;
    [SerializeField] private Collider _playerCollider;
    [SerializeField] private Camera _cam;
    [SerializeField] private float _gravity = -9.81f * 2;
    [SerializeField] private float _speed = 4f;
    [SerializeField] private float _sprintSpeed = 6.5f;
    [SerializeField] private float _crouchSpeed = 2.5f;
    [SerializeField] private float _jumpHeight = 1.5f;
    [SerializeField] private float groundDistance = 0.3f;
    [SerializeField] private Transform _groundCheck;

    private LayerMask _groundMask;
    private Vector3 _velocity;
    private bool _isGrounded;
    private bool _disabled;
    private SmoothCrouching _smoothCrouching;

    [HideInInspector] public bool Stop = false;


    private void Start()
    {
        _groundMask = LayerMask.GetMask("Ground");
        _disabled = false;
        _smoothCrouching = new SmoothCrouching(_playerCollider);
    }

    private void Update()
    {
        if (_disabled) return;

        //verify if player is on ground (aproximation)
        _isGrounded = Physics.CheckSphere(_groundCheck.position, groundDistance, _groundMask);

        HandleInput();

    }

    private void FixedUpdate()
    {
        #region gravity
        /*velocity.y = -2f to force player to slowly touch the ground, 
        because isGrounded uses an aproximate value to determine when the player is grounded
        whereas we want an exact value*/
        if (_isGrounded && _velocity.y < 0)
        {
            _velocity.y = -2f;
        }

        _velocity.y += _gravity * Time.deltaTime;

        //fall velocity = g * t^2
        _controller.Move(_velocity * Time.deltaTime);
        #endregion

        _smoothCrouching.Update();
    }

    private void HandleInput()
    {

        #region basicMovement
        //player movement in x and z axis----
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        //Combine the forward/backward and left/right movement
        Vector3 move = transform.right * x + transform.forward * z;

        //If you are crouching, you can't sprint.
        if (Input.GetButton("Crouch"))
        {
            _controller.Move(move * _crouchSpeed * Time.deltaTime);
        }
        else if (Input.GetButton("Sprint"))
        {
            _controller.Move(move * _sprintSpeed * Time.deltaTime);
        }
        else
        {
            _controller.Move(move * _speed * Time.deltaTime);
        }
        #endregion

        //we can jump as long as we are grounded. Yes even when crouching, with default behaviour. 
        if (Input.GetButtonDown("Jump") && _isGrounded)
        {
            Jump();
        }

        //we can crouch at any point
        if (Input.GetButtonDown("Crouch") || Input.GetButtonUp("Crouch"))
        {
            Crouch();
        }

    }

    private void Jump()
    {
        _velocity.y = Mathf.Sqrt(_jumpHeight * -2f * _gravity);
    }

    private void Crouch()
    {
        _smoothCrouching.ChangeCrouching();
    }

    public void ChangeDisabled()
    {
        _disabled = !_disabled;
    }
}
